# Advent of Code 2021

## build using nix

```
$ nix develop
$ cmake -Bbuild -S. -GNinja -DCMAKE_BUILD_TYPE=ASanAndUBSan
$ cmake --build build && cmake --build build --target test
```

