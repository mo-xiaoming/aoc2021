#include "common.hpp"

using namespace std::literals;

namespace {
enum class Binary : uint8_t { Zero, One };
using BinaryString = std::basic_string<Binary>;

auto read_day03_input() -> std::vector<BinaryString> {
  return read_input<BinaryString>(
      get_day_input(3),
      [](std::string_view s) {
        constexpr auto c2b = [](char c) {
            switch (c) {
            case '0': return Binary::Zero;
            case '1': return Binary::One;
            default:  throw InvalidInput("not one nor zero"sv);
            }
        };
        return s | rv::transform(c2b) | rg::to<BinaryString>;
      }
  );
};

template <rg::input_range Rng>
requires std::same_as<RngValueT<Rng>, Binary>
[[nodiscard]] auto bin2num(Rng&& rng) {
  auto result = 0U;
  for (auto [i, _] : rv::enumerate(rng | rv::reverse) | rv::filter([](auto p) {return p.second == Binary::One; })) {
    result += (1U << i);
  }
  return result;
}

template <rg::input_range Rng>
requires std::is_signed_v<RngValueT<Rng>>
[[nodiscard]] auto make_gamma(Rng&& rng) {
  return rng | rv::transform([](auto i) { return i > 0 ? Binary::One : Binary::Zero; });
}

template <rg::input_range Rng>
requires std::same_as<RngValueT<Rng>, Binary>
[[nodiscard]] auto neg_bin(Rng&& rng) {
  return rng | rv::transform([](Binary b) {
    switch (b) {
    case Binary::One:  return Binary::Zero;
    case Binary::Zero: return Binary::One;
    }
    throw InvalidInput("impossible"sv);
  });
}
} // namespace

TEST_CASE("Day 3, Part 1") {
  const auto input = read_day03_input();
  const auto result = [&input] {
    auto r = std::basic_string<int>(input[0].size(), 0);
    for (const auto &s : input) {
      for (const auto [i, c] : s | rv::enumerate) {
        switch (c) {
        case Binary::Zero: --r[i]; break;
        case Binary::One:  ++r[i]; break;
        }
      }
    }
    if (rg::contains(r, 0)) {
      throw InvalidInput("1s == 0s"sv);
    }
    return r;
  }(); // result = [-13, 9, 7, -5...]
  const auto gamma = make_gamma(result); // gamma =   [Z, O, O, Z...]
  const auto epsilon = neg_bin(gamma);   // epsilon = [O, Z, Z, O...]
  const std::unsigned_integral auto c = bin2num(gamma) * bin2num(epsilon);
  CHECK_EQ(c, 3'847'100U);
}

TEST_CASE("Day 3, Part 2") {
  const auto input = read_day03_input();
  using Flags = std::basic_string<bool>;

  const auto get_entry_idx = [&input]<typename Fn>(Fn&& fn) {
    const auto enabled_bs = [](const Flags& mask) {
      return [mask](const auto &p) { return mask[p.first]; };
    };
    const auto get_bs_col_c = [](auto col) {
      return [col](const auto &p) { return p.second[col]; };
    };
    auto enabled = Flags(input.size(), true);
    for (auto col = 0U; col < input[0].size(); ++col) {
       auto cs_on_col = rv::enumerate(input) | rv::filter(enabled_bs(enabled))
                                             | rv::transform(get_bs_col_c(col))
                                             | rg::to<BinaryString>;
       const std::signed_integral auto bc = std::accumulate(
           cs_on_col.cbegin(), cs_on_col.cend(),
           0,
           [](int acc, Binary b) {
               switch (b) {
               case Binary::Zero: return acc - 1;
               case Binary::One:  return acc + 1;
               }
               throw InvalidInput("impossible"sv);
           });
       for (auto r = 0U; r < enabled.size(); ++r) {
         if (enabled[r]) {
           enabled[r] = input[r][col] == (std::forward<Fn>(fn)(bc, 0) ? Binary::One : Binary::Zero);
         }
       }
       if (rg::count(enabled, true) == 1) {
         break;
       }
    }
    if (rg::count(enabled, true) == 0) {
      throw InvalidInput("something is wrong"sv);
    }
    return static_cast<std::size_t>(rg::distance(rg::begin(enabled), rg::find(enabled, true)));
  };
  const auto oxygen_rating = input[get_entry_idx(rg::greater_equal())];
  const auto co2_rating    = input[get_entry_idx(rg::less())];
  const std::unsigned_integral auto c = bin2num(oxygen_rating) * bin2num(co2_rating); 
  CHECK_EQ(c, 4'105'235U);
}
