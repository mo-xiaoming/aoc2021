#pragma once

#include <exception>
#include <concepts>
#include <execution>
#include <numeric>
#include <fstream>
#include <charconv>
#include <cstdlib>

#include <fmt/printf.h>
#include <range/v3/all.hpp>
#include <doctest/doctest.h>

namespace rg = ranges;
namespace rv = ranges::views;

[[nodiscard]] inline auto get_day_input(int day) -> std::string {
  if (char *p = std::getenv("AOC_INPUTS_DIR"); p != nullptr) { // NOLINT
    return fmt::format("{}/Day{:02}.txt", p, day);
  }
  return fmt::format("./src/inputs/Day{:02}.txt", day);
}

struct InvalidInput : std::exception {
  explicit InvalidInput(std::string_view input): input_(input) {}
  explicit InvalidInput(std::string input): input_(std::move(input)) {}
  [[nodiscard]] auto what() const noexcept -> const char * override { return input_.c_str(); }
private:
  const std::string input_;
};

template <typename T>
[[nodiscard]] auto to_num(std::string_view s) -> T {
  auto result = T();
  const auto *const end = s.data() + s.size();
  const auto [ptr, ec] = std::from_chars(s.data(), end, result);
  if (ptr == end && ec == std::errc()) {
    return result;
  }
  throw InvalidInput(fmt::format("to_num: {}", s));
}

template <rg::input_range Rng>
using RngValueT = rg::range_value_t<std::decay_t<Rng>>;

inline constexpr auto trim_front = rv::drop_while(::isspace);
inline constexpr auto trim_back = rv::reverse | trim_front | rv::reverse;
inline constexpr auto trim = trim_front | trim_back;
[[nodiscard]] inline auto trim_str(std::string_view s) -> std::string {
  return s | trim | rg::to<std::string>();
}

[[nodiscard]] inline auto split_by_delim(std::string_view s, char d=' ') -> std::vector<std::string> {
  return s | rv::split(d)
           | rv::filter([](auto r) { return ! r.empty(); })
           | rg::to<std::vector<std::string>>;
};

template <typename Fn, typename Ret>
concept LineConverterFn = requires(Fn f) {
  { f(std::string_view()) } -> std::same_as<Ret>;
};

[[nodiscard]] inline auto default_line_converter(std::string_view s) { return std::string(s); }

template <typename T, typename LineConverter = decltype(default_line_converter)>
requires LineConverterFn<LineConverter, T>
[[nodiscard]] auto read_input(const std::string& filename, LineConverter&& fn = default_line_converter) -> std::vector<T> {
  auto v = std::vector<T>();
  std::ifstream f(filename);
  if (!f) {
    throw InvalidInput(fmt::format("couldn't open {}\n", filename));
  }
  std::string trimmed;
  std::string line;
  while (std::getline(f, line)) {
    trimmed = trim_str(line);
    if (!trimmed.empty()) {
      v.push_back(std::forward<LineConverter>(fn)(trimmed));
    }
  }
  return v;
}

