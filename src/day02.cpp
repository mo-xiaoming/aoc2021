#include "common.hpp"

using namespace std::literals;

namespace {
enum class Direction : std::int8_t { Forward, Down, Up };

auto to_direction(std::string_view s) -> Direction {
  static constexpr auto m = std::array{
    std::make_pair("forward"sv, Direction::Forward),
    std::make_pair("down"sv, Direction::Down),
    std::make_pair("up"sv, Direction::Up),
  };
  if (const auto it = rg::find(m, s, &decltype(m)::value_type::first); it != rg::end(m)) {
    return it->second;
  }
  throw InvalidInput(s);
}

struct Action {
  Direction direction;
  int units;
};

auto read_day02_input() -> std::vector<Action> {
  return read_input<Action>(
      get_day_input(2),
      [](std::string_view s) {
        const auto tokens = split_by_delim(s);
        if (tokens.size() != 2) {
          throw InvalidInput(s);
        }
        return Action{.direction = to_direction(tokens[0]), .units = to_num<int>(tokens[1])};
      }
  );
};
} // namespace

TEST_CASE("Day 2, Part 1") {
  const auto input = read_day02_input();
  int h = 0;
  int v = 0;
  for (const auto &a : input) {
    switch (a.direction) {
    case Direction::Down:    v += a.units; break;
    case Direction::Up:      v -= a.units; break;
    case Direction::Forward: h += a.units; break;
    }
  }
  CHECK_EQ(h * v, 1'488'669);
}

TEST_CASE("Day 2, Part 2") {
  const auto input = read_day02_input();
  int h = 0;
  int v = 0;
  int aim = 0;
  for (const auto &a : input) {
    switch (a.direction) {
    case Direction::Down: aim += a.units; break;
    case Direction::Up:   aim -= a.units; break;
    case Direction::Forward: h += a.units; v += aim * a.units; break;
    }
  }
  CHECK_EQ(h * v, 1'176'514'794);
}
