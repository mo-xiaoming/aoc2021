#include "common.hpp"

namespace {
template <rg::bidirectional_range Rng>
[[nodiscard]] auto count_adjacent_less(Rng &&rng) -> int {
  using ET = RngValueT<Rng>;
  return std::transform_reduce(
      std::execution::par_unseq,
      rg::begin(std::forward<Rng>(rng)), rg::prev(rg::end(std::forward<Rng>(rng))),
      rg::next(rg::begin(std::forward<Rng>(rng))),
      ET(),
      rg::plus(),
      [](ET a, ET b) { return a < b ? 1 : 0; }
   );
}

auto read_day01_input() {
  return read_input<int>(
      get_day_input(1),
      [](std::string_view s) { return to_num<int>(s); }
  );
};
} // namespace

TEST_CASE("Day 1, Part 1") {
  const auto input = read_day01_input();
  const std::signed_integral auto c = count_adjacent_less(input);
  CHECK_EQ(c, 1'233);
}

TEST_CASE("Day 1, Part 2") {
  const auto input = read_day01_input();
  auto rng = input | rv::sliding(3) | rv::transform(
      [](auto r) { return rg::accumulate(r, 0); }
  );
  const std::signed_integral auto c = count_adjacent_less(rng);
  CHECK_EQ(c, 1'275);
}

