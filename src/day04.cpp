#include "common.hpp"

#include <unordered_set>

using namespace std::literals;

namespace {
constexpr auto Cols = 5;
constexpr auto Rows = 5;

struct Board {
private:
  struct Pos {
    int value = -1;
    bool marked = false;
  };
  std::array<Pos, static_cast<std::size_t>(Rows * Cols)> pos_;
public:
  template <rg::input_range Rng>
  explicit Board(const Rng &rng) {
    for (auto [i, v] : rv::enumerate(rng)) {
      pos_[i] = {.value = v, .marked = false};
    }
  }
  constexpr void set(int v) {
    if (auto it = rg::find(rg::begin(pos_), rg::end(pos_), v, &Pos::value); it != rg::end(pos_)) {
      *it = {.value = v, .marked = true};
    }
  }
  [[nodiscard]] auto hasWon() const -> bool {
    // vertical
    for (std::decay_t<decltype(Cols)> n = {}; n < Cols; ++n) {
      bool win = rg::accumulate(pos_ | rv::drop(n) | rv::stride(Cols), true, std::logical_and<>(), &Pos::marked);
      if (win) {
        return true;
      }
    }
    // horizontal
    for (auto line : pos_ | rv::chunk(Cols)) {
      bool win = rg::accumulate(line, true, std::logical_and<>(), &Pos::marked);
      if (win) {
        return true;
      }
    }

    return false;
  }
  [[nodiscard]] auto sum_unmask() const {
    return rg::accumulate(pos_ | rv::filter([](Pos p) { return !p.marked; }), 0, {}, &Pos::value);
  }

  [[maybe_unused]] friend auto operator<<(std::ostream& os, const Board& b) -> std::ostream& {
    rg::for_each(b.pos_ | rv::chunk(Cols), [&os](auto r) {
        rg::for_each(r, [&os](auto p) {
            os << fmt::format("{:4d}{}", p.value, (p.marked ? 'x' : ' '));
        });
        os << '\n';
    });
    return os;
  }
};

auto read_day04_input() -> std::pair<std::vector<int>, std::vector<Board>> {
  const auto text = read_input<std::string>(get_day_input(4));
  const auto data = [](std::string_view s) {
    const auto tokens = split_by_delim(s, ',');
    return tokens | rv::transform(to_num<int>) | rg::to<std::vector<int>>;
  }(text[0]);
  const auto boards = [&text] {
    auto result = std::vector<Board>();
    result.reserve(text.size() / Rows);
    for (auto blk : text | rv::drop(1) | rv::chunk(Rows)) {
      auto rng = blk | rv::transform([](auto line) {
          const auto tokens = split_by_delim(line);
          return tokens | rv::transform(to_num<int>) | rg::to<std::vector<int>>;
      });
      auto d = rg::accumulate(rng, std::vector<int>(), [](std::vector<int> acc, const std::vector<int>& e) {
          acc.insert(acc.cend(), e.cbegin(), e.cend());
          return acc;
      });
      result.emplace_back(d);
    }
    return result;
  }();
  return {data, boards};
};
} // namespace

TEST_CASE("Day 4, Part 1") {
  auto [data, boards] = read_day04_input();
  const auto find_num_and_board = [&boards=boards, &data=std::as_const(data)]() -> std::pair<int, Board> {
    for (const auto num : data) {
      for (auto &b : boards) {
        b.set(num);
        if (b.hasWon()) {
          return {num, b};
        }
      }
    }
    throw InvalidInput("couldn't find board"sv);
  };
  const auto &[num, board] = find_num_and_board();
  CHECK_EQ(num * board.sum_unmask(), 10'680);
}

TEST_CASE("Day 4, Part 2") {
  auto [data, boards] = read_day04_input();
  const auto find_num_and_board = [&boards=boards, &data=data]() -> std::pair<int, Board> {
    struct {
      std::unordered_set<typename std::decay_t<decltype(boards)>::size_type> won;
      typename std::decay_t<decltype(boards)>::size_type last_won{};
      typename std::decay_t<decltype(data)>::value_type num{};
    } pt;
    for (const auto num : data) {
      for (typename std::decay_t<decltype(boards)>::size_type i{}; i < boards.size(); ++i) {
        if (pt.won.contains(i)) {
          continue;
        }
        boards[i].set(num);
        if (boards[i].hasWon()) {
          pt.last_won = i;
          pt.num = num;
          pt.won.insert(i);
        }
      }
    }
    return {pt.num, boards[pt.last_won]};
  };
  const auto &[num, board] = find_num_and_board();
  CHECK_EQ(num * board.sum_unmask(), 31'892);
}
